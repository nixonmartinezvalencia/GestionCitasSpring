package com.quileia;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.quileia.entity.Usuario;
import com.quileia.services.UserService;
import com.quileia.security.model.PermissionDTO;
import com.quileia.security.model.RoleDTO;

@SpringBootTest
class TestUser {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userService;

	@Test
	void insercionCita() {

		Usuario user = new Usuario();
		user.setUsername("nixon5martinezvalencia@hotmail.com");
		user.setPassword(passwordEncoder.encode("123456"));
		user.setEnable(true);
		Set<PermissionDTO> permissions = new HashSet<>();

		permissions.add(new PermissionDTO("CRUD Usuarios"));
		permissions.add(new PermissionDTO("Autor"));
		permissions.add(new PermissionDTO("Editor"));

		user.setRol(new RoleDTO("Admin General", true, permissions));

		assertTrue(userService.createUser(user) != null);

	}

}

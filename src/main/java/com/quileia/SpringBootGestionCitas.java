package com.quileia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.quileia" })
public class SpringBootGestionCitas {
	

	public static void main(String[] args) {
		SpringApplication.run(SpringBootGestionCitas.class,args);

	}

}

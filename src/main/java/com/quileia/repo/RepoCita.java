package com.quileia.repo;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.quileia.entity.EntidadCita;

@Repository
public interface RepoCita
		extends MongoRepository<EntidadCita, String>, PagingAndSortingRepository<EntidadCita, String> {

	public Page<EntidadCita> findAll(Pageable pageable);

	@Query("{'horarioCitaEntidad':?0,'pacienteEntidad':?1}")
	public abstract Optional<EntidadCita> getDisponibilidadPaciente(String horarioCita,
			String paciente);

	@Query("{'horarioCitaEntidad':?0,'medicoEntidad':?1}")
	public abstract Optional<EntidadCita> getDisponibilidadMedico(String horarioCita, String medico);

}

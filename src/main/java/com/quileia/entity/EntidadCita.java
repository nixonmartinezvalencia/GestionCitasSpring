package com.quileia.entity;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "EntidadCita")
public class EntidadCita {

	@Id
	private String registroEntidad;

	private String pacienteEntidad;

	private String medicoEntidad;

	private String horarioCitaEntidad;

	public String getPacienteEntidad() {
		return pacienteEntidad;
	}

	public void setPacienteEntidad(String pacienteEntidad) {
		this.pacienteEntidad = pacienteEntidad;
	}

	public String getMedicoEntidad() {
		return medicoEntidad;
	}

	public void setMedicoEntidad(String medicoEntidad) {
		this.medicoEntidad = medicoEntidad;
	}

	public String getHorarioCitaEntidad() {
		return horarioCitaEntidad;
	}

	public void setHorarioCitaEntidad(String horarioCitaEntidad) {
		this.horarioCitaEntidad = horarioCitaEntidad;
	}

	public String getRegistroEntidad() {
		return registroEntidad;
	}

	public void setRegistroEntidad(String registroEntidad) {
		this.registroEntidad = registroEntidad;
	}

}

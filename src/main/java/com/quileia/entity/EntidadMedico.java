package com.quileia.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "EntidadMedico")
public class EntidadMedico {

	private String nombreEntidad;

	private String apellidoEntidad;

	@Id
	private String identificacionEntidad;

	private String tipoIdentificacionEntidad;

	private String numeroTarjetaProfesionalEntidad;

	private Float tiempoExperienciaEntidad;

	private String especialidadEntidad;

	private String inicioAtencionEntidad;

	private String finAtencionEntidad;

	public String getNombreEntidad() {
		return nombreEntidad;
	}

	public void setNombreEntidad(String nombreEntidad) {
		this.nombreEntidad = nombreEntidad;
	}

	public String getApellidoEntidad() {
		return apellidoEntidad;
	}

	public void setApellidoEntidad(String apellidoEntidad) {
		this.apellidoEntidad = apellidoEntidad;
	}

	public String getIdentificacionEntidad() {
		return identificacionEntidad;
	}

	public void setIdentificacionEntidad(String identificacionEntidad) {
		this.identificacionEntidad = identificacionEntidad;
	}

	public String getTipoIdentificacionEntidad() {
		return tipoIdentificacionEntidad;
	}

	public void setTipoIdentificacionEntidad(String tipoIdentificacionEntidad) {
		this.tipoIdentificacionEntidad = tipoIdentificacionEntidad;
	}

	public String getNumeroTarjetaProfesionalEntidad() {
		return numeroTarjetaProfesionalEntidad;
	}

	public void setNumeroTarjetaProfesionalEntidad(String numeroTarjetaProfesionalEntidad) {
		this.numeroTarjetaProfesionalEntidad = numeroTarjetaProfesionalEntidad;
	}

	public Float getTiempoExperienciaEntidad() {
		return tiempoExperienciaEntidad;
	}

	public void setTiempoExperienciaEntidad(Float tiempoExperienciaEntidad) {
		this.tiempoExperienciaEntidad = tiempoExperienciaEntidad;
	}

	public String getEspecialidadEntidad() {
		return especialidadEntidad;
	}

	public void setEspecialidadEntidad(String especialidadEntidad) {
		this.especialidadEntidad = especialidadEntidad;
	}

	public String getInicioAtencionEntidad() {
		return inicioAtencionEntidad;
	}

	public void setInicioAtencionEntidad(String inicioAtencionEntidad) {
		this.inicioAtencionEntidad = inicioAtencionEntidad;
	}

	public String getFinAtencionEntidad() {
		return finAtencionEntidad;
	}

	public void setFinAtencionEntidad(String finAtencionEntidad) {
		this.finAtencionEntidad = finAtencionEntidad;
	}

}

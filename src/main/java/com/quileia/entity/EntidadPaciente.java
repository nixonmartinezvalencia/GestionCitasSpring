package com.quileia.entity;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "EntidadPaciente")
public class EntidadPaciente {

	private String nombreEntidad;

	private String fechaNacimientoEntidad;

	@Id
	private String identificacionEntidad;

	private String tipoIdentificacionEntidad;

	private String epsEntidad;

	private String historiaClinicaEntidad;

	public EntidadPaciente() {
	}

	public EntidadPaciente(String nombre, String fechaNacimiento, String identificacion, String tipoIdentificacion,
			String eps, String historiaClinica) {

		this.nombreEntidad = nombre;
		this.fechaNacimientoEntidad = fechaNacimiento;
		this.identificacionEntidad = identificacion;
		this.tipoIdentificacionEntidad = tipoIdentificacion;
		this.epsEntidad = eps;
		this.historiaClinicaEntidad = historiaClinica;
	}

	public String getNombreEntidad() {
		return nombreEntidad;
	}

	public void setNombreEntidad(String nombreEntidad) {
		this.nombreEntidad = nombreEntidad;
	}

	public String getFechaNacimientoEntidad() {
		return fechaNacimientoEntidad;
	}

	public void setFechaNacimientoEntidad(String fechaNacimientoEntidad) {
		this.fechaNacimientoEntidad = fechaNacimientoEntidad;
	}

	public String getIdentificacionEntidad() {
		return identificacionEntidad;
	}

	public void setIdentificacionEntidad(String identificacionEntidad) {
		this.identificacionEntidad = identificacionEntidad;
	}

	public String getTipoIdentificacionEntidad() {
		return tipoIdentificacionEntidad;
	}

	public void setTipoIdentificacionEntidad(String tipoIdentificacionEntidad) {
		this.tipoIdentificacionEntidad = tipoIdentificacionEntidad;
	}

	public String getEpsEntidad() {
		return epsEntidad;
	}

	public void setEpsEntidad(String epsEntidad) {
		this.epsEntidad = epsEntidad;
	}

	public String getHistoriaClinicaEntidad() {
		return historiaClinicaEntidad;
	}

	public void setHistoriaClinicaEntidad(String historiaClinicaEntidad) {
		this.historiaClinicaEntidad = historiaClinicaEntidad;
	}

}
